<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>定数を使用するには？</title>
    </head>
    <body>
        <?php
       //定数を定義します
       define('HELLO', 'Hello world!');
       echo '定数:HELLO' . HELLO .'<br>';
       //定数Helloは定義されていないためNoticeエラーが発生します 
       echo '定数:Hello' . Hello .'<br>';
       //定数定義の戻り値を$resultに代入します。第3引数にtrueを指定して大文字・小文字区別しないようにします
$result = define("GREETING", 'こんにちは', true); 
if ($result) {
    echo '定数:GREETING'. GREETING .'<br>';
    echo '定数:Greeting'. greeting .'<br>';  
}else{
    echo '定数の定義に失敗しました' . '<br>';
}

       
        ?>
    </body>
</html>
