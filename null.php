<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>null</title>
    </head>
    <body>
        <?php
$a = null;
$b = '';
//どちらも値は表示されません
echo '$a' . $a . '<br>';
echo '$b' . $b . '<br>';

if($a==$b){
    echo '$aと$bは等しい(==)<br>';
}else{
    echo '$aと$bは異なる(==)<br>';
}

if($a===$b){
    echo '$aと$bは等しい(===)<br>';
}else{
    echo '$aと$bは異なる(===)<br>';
}

if(is_null($a)){
    echo '$aはnullです<br>';
}else{
    echo '$aはnullではありません<br>';
}

if(isset($a)){
    echo '$aは定義されています<br>';
}else{
    echo '$aは定義されていません<br>';
}
        ?>
    </body>
</html>
