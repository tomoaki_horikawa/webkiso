<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $people = array(
             array('name' => 'suzuki',
                   'height' => 179,
            ),
            array(
                'name' => 'kimura',
                'height' => 167, 
            ),
            array(
                'name' => 'miura',
                'height' => 192, 
            )    
            );    
                
//        echo '<table>';
//        echo '<thead>';
//        echo '<tr>';
//        echo '<td>名前</td>';
//        echo '<td>身長</td>';
//        echo '</tr>';
//        echo '</thead>';
//        echo '<tbody>';
//        foreach ($people as $parson){
//        echo '<tr>';
//        echo '<td>'.$parson['name'].'</td>';
//        echo '<td>'.$parson['height'].'</td>';
//        echo '</tr>';
//        }
//        echo '</tbody>';
//        echo '</table>';
        ?>
        <table>
            <thead>
                <tr><th>名前</th><th>身長</th></tr>   
            </thead>
            <tbody>
                <?php foreach ($people as $person):?>
                <tr>
                    <td><?=$person['name'];?></td>
                    <td><?=$person['height'];?></td> 
                </tr> 
                <?php endforeach; ?>
            </tbody>
        </table> 
    </body>
</html>
