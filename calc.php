<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <title>計算</title>
</head>
<body>
<?php
//四則演算を行います
$a = 10;
$b = 5;

echo $a + $b . '<br>';
echo $a - $b . '<br>';
echo $a * $b . '<br>';
echo $a / $b . '<br><br>';

//'%' 演算子  
echo $a % $b . '<br>';
echo $b % 3 . '<br><br>';

// ++, --
echo $a++ . '<br>';
echo ++$a . '<br>';
echo $b-- .'<br>';
echo --$b . '<br>';  
?>
</body>
</html>
