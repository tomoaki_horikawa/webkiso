<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
         function three_five($number){
            
              if($number%3 == 0 && $number%5 == 0){
                  return 'fifteen';
            }elseif ($number%5 == 0) {
                return 'five';
        
            }elseif($number%3 == 0){
                return 'three';
            }else{
              return $number; 
            }
            
        } 
        
        echo three_five(1).' ';
        echo three_five(2).' ';
        echo three_five(3).' ';
        echo '<br>';
        for($i=1; $i<=100; $i++){
            echo three_five($i).' ';
        }
        ?>
    </body>
</html>
