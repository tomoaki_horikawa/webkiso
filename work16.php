<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $result = 0;
        $result_even = 0;
        for($i=1; $i<=100; $i++){
            
            $result += $i;
            if($i%2 == 0){
                $result_even += $i;
            }
        };
        
        echo '1〜100までのすべての数字を足すと'. $result.'<br>';
        echo '1〜100までのすべての偶数を足すと'. $result_even;
        ?>
    </body>
</html>
