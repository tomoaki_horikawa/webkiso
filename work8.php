<!DOCTYPE html>

<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo date('Y/m/d H:i:s',300000);
        echo '<br>';
        echo date('Y/m/d H:i:s',0);
        echo '<br>';
        
        function is_sunday($timestamp = null){
            //$timestampが渡されてなかったら
            //現在のタイムスタンプを代入
            
            if ($timestamp === null){
                $timestamp = time();
            }
            
            $youbi = date('w',$timestamp);
            if($youbi ==  0){
                echo date('Y/m/d H:i:s',$timestamp).'は日曜日です<br>';
            } else {
                echo date('Y/m/d H:i:s',$timestamp).'日曜日ではありません<br>';
             }
        }
        
        is_sunday(0);
        is_sunday(100000);
        is_sunday(200000);
        is_sunday(300000); 
        is_sunday(400000);
        is_sunday(500000);
        is_sunday(600000);
        is_sunday(900000);
        is_sunday(1000000);
        is_sunday(1100000);
        is_sunday(1200000);
        is_sunday(1300000);
        is_sunday(1400000);
        is_sunday(1500000);
        $timestamp = time();
        echo $timestamp.'<br>';
        is_sunday($timestamp);
        
        $timestamp2 = mktime(0,0,0,6,18,2017);
        echo $timestamp2.'<br>';
        is_sunday();
         
//        function what_day(){
//        $youbi = date('w');
//            if($youbi == 0){
//                echo '日曜日<br>';
//            }elseif($youbi == 1){
//                echo '月曜日<br>';
//            }elseif($youbi == 2){
//                echo '火曜日<br>';
//            }elseif($youbi == 3){
//                echo '水曜日<br>';
//            }elseif($youbi == 4){
//                echo '木曜日<br>';
//            }elseif($youbi == 5){
//                echo '金曜日<br>';
//            }elseif($youbi == 6){
//                echo '土曜日<br>';
//                }
//        }
//        
//        what_day();
        ?>
    </body>
</html>
